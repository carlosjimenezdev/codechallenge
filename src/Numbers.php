<?php

	class Numbers {

		protected $numbers;
		protected $multiplesOfFirst;
		protected $multiplesOfSecond;

		public function setFirstNumber($number) {
			$this->firstNumber	= $number;
		}

		public function setFirstWord($word) {
			$this->firstWord	= $word;
		}

		public function setSecondNumber($number) {
			$this->secondNumber	= $number;
		}

		public function setSecondWord($word) {
			$this->secondWord	= $word;
		}

		public function setBothWord($word) {
			$this->bothWord	= $word;
		}

		public function getNumbers($first,$second,$both) {

			$this->setFirstNumber($first['number']);
			$this->setFirstWord($first['word']);
			$this->setSecondNumber($second['number']);
			$this->setSecondWord($second['word']);
			$this->setBothWord($both['word']);
			
			$this->numbers 				= $this->get100Numbers();
			$this->multiplesOfFirst 	= $this->getMultiplesOfFirst($this->numbers);
			$this->multiplesOfSecond 	= $this->getMultiplesOfSecond($this->multiplesOfFirst);
			$this->printResult($this->multiplesOfSecond);

		}

		public function get100Numbers() {
			$arr = [];

			for($i=0; $i <= 100; $i++) {
				$arr[] = $i;
			}

			return $arr;
		}

		public function getMultiplesOfFirst($arr) {

			for($i=0; $i <= 100; $i+=$this->firstNumber) {
				$arr[$i] = $this->firstWord;
			}

			return $arr;

		}

		public function getMultiplesOfSecond($arr) {

			for($i=0; $i <= 100; $i+=$this->secondNumber) {
				switch($arr[$i]) {
					case $this->firstWord:
						$arr[$i] = $this->bothWord;
					break;
					default:
						$arr[$i] = $this->secondWord;
					break;
				}
			}

			return $arr;

		}

		public function printResult($arr) {
			unset($arr[0]);

			foreach($arr as $a) {
				echo $a.'<br />';
			}
		}
	}

?>