<?php

	use PHPUnit\Framework\TestCase;

	class NumbersTest extends TestCase {
		protected $Numbers;

		public function setUp():void {

			$this->Numbers 		= new Numbers();
			$this->firstNumber	= 3;
			$this->firstWord	= 'Linio';
			$this->secondNumber	= 5;
			$this->secondWord	= 'IT';
			$this->bothWord		= 'Linieros';

		}

	    public function testNumbersCreation() {
	    	$result = count($this->Numbers->get100Numbers());
	    	$expected = 101;
	        
	        $this->assertEquals($expected,$result);
	    }

	    public function testMultiplesFromFirst() {

	    	$this->Numbers->setFirstNumber($this->firstNumber);
	    	$this->Numbers->setFirstWord($this->firstWord);

	    	$expected = 0;

	    	for($i=0; $i <= 100; $i+=$this->firstNumber) {
	    		if($i % $this->firstNumber == 0) {
					$expected++;
				}
			}

	    	$result = 0;
	    	$testNumbers 			= $this->Numbers->get100Numbers();
	    	$getMultiplesOfFirst 	= $this->Numbers->getMultiplesOfFirst($testNumbers);

	    	foreach($getMultiplesOfFirst as $g) {
	    		if($g == $this->firstWord) {
	    			$result++;
	    		}
	    	}
	        
	        $this->assertEquals($expected,$result);
	    }

	    public function testMultiplesFromSecond() {

	    	$this->Numbers->setFirstNumber($this->firstNumber);
	    	$this->Numbers->setFirstWord($this->firstWord);
	    	$this->Numbers->setSecondNumber($this->secondNumber);
	    	$this->Numbers->setSecondWord($this->secondWord);
	    	$this->Numbers->setBothWord($this->bothWord);

	    	$expected = 0;

	    	for($i=0; $i <= 100; $i+=$this->secondNumber) {
	    		if($i % $this->secondNumber == 0) {
					$expected++;
				}
				if($i % $this->firstNumber == 0) {
					$expected--;
				}
			}

	    	$result = 0;
	    	$testNumbers 			= $this->Numbers->get100Numbers();
	    	$getMultiplesOfFirst 	= $this->Numbers->getMultiplesOfFirst($testNumbers);
	    	$getMultiplesOfSecond 	= $this->Numbers->getMultiplesOfSecond($getMultiplesOfFirst);

	    	foreach($getMultiplesOfSecond as $g) {
	    		if($g == $this->secondWord) {
	    			$result++;
	    		}
	    	}
	        
	        $this->assertEquals($expected,$result);
	    }

	    public function tearDown():void {
	    	$this->Numbers 		= null;
	    	$this->firstNumber	= null;
			$this->firstWord	= null;
			$this->secondNumber	= null;
			$this->secondWord	= null;
			$this->bothWord		= null;
	    }
	}

?>